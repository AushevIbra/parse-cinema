<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use App\QueryFilters\CinemaFilter;
use App\Services\ParseCinemaService;
use Illuminate\Http\Request;

class CinemaController extends Controller
{
    /**
     * @var ParseCinemaService
     */
    private $service;

    public function __construct(ParseCinemaService $service)
    {
        $this->service = $service;
    }


    public function get(Request $request, CinemaFilter $filter)
    {
        $model = Cinema::filter($filter)->paginate(10);
        return view('index', [
            'model' => $model
        ]);
    }

    public function update()
    {
        try {
            $this->service->parse();
            return redirect()->route('index');
        } catch (\Throwable $exception) {
            echo "Возникла непредвиденная ошибка";
        }
    }
}
