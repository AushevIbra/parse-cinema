<?php
declare(strict_types=1);

namespace App\QueryFilters;


use App\Models\Cinema;
use Carbon\Carbon;

class CinemaFilter extends QueryFilter
{

    public function name(?string $value)
    {
        if (!$value) {
            return;
        }

        $this->builder->where(Cinema::TABLE_NAME . '.' . Cinema::ATTR_NAME, 'LIKE', "%$value%");
    }

    public function id(?string $value)
    {
        if (!$value) {
            return;
        }

        $this->builder->where(Cinema::TABLE_NAME . '.' . Cinema::ATTR_ID, $value);
    }

    public function price(?string $value)
    {
        if (!$value) {
            return;
        }

        $this->builder->where(Cinema::TABLE_NAME . '.' . Cinema::ATTR_PRICE, $value);
    }

    public function session_date(?string $value)
    {

        if (!$value) {
            return;
        }

        $this->builder->where(Cinema::TABLE_NAME . '.' . Cinema::ATTR_SESSION_DATE, date("Y-m-d h:i:s", strtotime($value)));
    }


}
