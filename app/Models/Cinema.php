<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Cinema
 * @package App\Models
 *
 * @property integer $id
 * @property string $name
 * @property string $session_date
 * @property integer $status
 * @property integer $price
 */
class Cinema extends Model
{
    use SoftDeletes;

    const TABLE_NAME = 'cinemas';

    const ATTR_ID           = 'id';
    const ATTR_NAME         = 'name';
    const ATTR_SESSION_DATE = 'session_date';
    const ATTR_STATUS       = 'status';
    const ATTR_PRICE        = 'price';


    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;

    protected $fillable = [self::ATTR_NAME, self::ATTR_SESSION_DATE, self::ATTR_STATUS, self::ATTR_PRICE];

    public static function getStatusVariants()
    {
        return [
            static::STATUS_INACTIVE => 'Выключен',
            static::STATUS_ACTIVE   => 'Включен'
        ];
    }

    public function scopeFilter($builder, $filters)
    {
        return $filters->apply($builder);
    }
}
