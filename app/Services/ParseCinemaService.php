<?php
/**
 * Created by PhpStorm.
 * User: aushev
 * Date: 09.10.2019
 * Time: 13:18
 */

namespace App\Services;


use App\Models\Cinema;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class ParseCinemaService
{
    const URL = 'https://cityopen.ru/afisha/kinoteatr-salavat/';

    public function parse()
    {
        $client = new Client([
            'http_errors' => false,
        ]);

        $response = $client->get(static::URL);

        if ($response->getStatusCode() == 200) {
            $html    = $response->getBody()->getContents();
            $crawler = new Crawler();
            $crawler->addHtmlContent($html);
            $date = $crawler->filter("table")->first()->filter('tbody > tr > td')->last()->text();

            $sessions = $crawler->filter("table")->each(function (Crawler $table, $key) use ($date) {
                if ($key !== 0) {
                    return $table->filter("tbody > tr")->each(function (Crawler $tr, $key) use ($date) {
                        if ($key !== 0) {
                            $time  = $tr->filter('td')->first()->text();
                            $name  = $tr->filter('td')->getNode(1)->textContent;
                            $price = $tr->filter('td')->last()->text();


                            $this->save([
                                Cinema::ATTR_NAME         => $name,
                                Cinema::ATTR_PRICE        => $price,
                                Cinema::ATTR_SESSION_DATE => $this->convertStringToDate($date) . ' ' . $time
                            ]);
                        }
                    });
                }
            });


        }
    }

    public function save(array $attributes)
    {
        Cinema::firstOrCreate($attributes);
    }

    /**
     * @param string $str
     * @return string
     */
    private function convertStringToDate(string $str): string
    {
        $mounth        = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        $mounts_string = [
            'января',
            'февраля',
            'марта',
            'апреля',
            'мая',
            'июня',
            'июля',
            'августа',
            'сентября',
            'октября',
            'ноября',
            'декабря',
        ];
        $tempDate      = explode(" ", $str);
        $date          = $tempDate[0] . "-" . str_replace($mounts_string, $mounth,
                mb_strtolower($tempDate[1])) . "-" . $tempDate[2];

        return date("Y-m-d", strtotime($date));
    }
}
