<?php
/**
 * @var \App\Models\Cinema[] $model
 */
?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{route('index')}}">
            <table class="table">
                <thead>
                <tr class="text-center">
                    <th>#</th>
                    <th>Название</th>
                    <th>Время</th>
                    <th>Цена</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr class="text-center">
                    <td>
                        <input name="id" class="form-control" type="text" value="{{request('id')}}">
                    </td>
                    <td>
                        <input name="name" class="form-control" type="text" value="{{request('name')}}">
                    </td>
                    <td>
                        <input name="session_date" class="form-control" type="datetime-local" value="{{request('session_date')}}">
                    </td>
                    <td>
                        <input class="form-control" type="text" value="{{request('price')}}" name="price">
                    </td>
                    <td>
                        <button type="submit" class="btn btn-primary">Применить</button>
                    </td>
                </tr>


                @foreach($model as $cinema)
                    <tr class="text-center">
                        <td>
                            <strong>{{$cinema->id}}</strong>
                        </td>

                        <td>
                            {{$cinema->name}}
                        </td>

                        <td>
                            {{$cinema->session_date}}
                        </td>

                        <td>
                            {{$cinema->price}}
                        </td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </form>
        {{$model->appends(request()->all())->links()}}
    </div>
@endsection
