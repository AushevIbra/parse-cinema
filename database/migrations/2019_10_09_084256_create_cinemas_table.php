<?php

use App\Models\Cinema;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCinemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cinemas', function (Blueprint $table) {
            $table->increments(Cinema::ATTR_ID);
            $table->string(Cinema::ATTR_NAME)->index();
            $table->timestamp(Cinema::ATTR_SESSION_DATE)->index();
            $table->integer('price')->index();
            $table->integer('status')->default(Cinema::STATUS_ACTIVE);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cinemas');
    }
}
